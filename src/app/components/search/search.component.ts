import { Component, OnInit } from '@angular/core';
import {SpotifyService} from '../../services/spotify.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styles: []
})
export class SearchComponent {
  artists: any[] = [];
  loading: boolean;
  error = false;
  msgError: string;

  constructor(private _spotify: SpotifyService) {}

  search(termino: string) {
    this.loading = true;

    this._spotify.getArtists(termino).subscribe(
      (response: any) => {
        this.artists = response;
        this.loading = false;
      },
      err => {
        this.error = true;
        this.msgError = err.error.error.message;
        this.loading = false;
      }
    );
  }
}
