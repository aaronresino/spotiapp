import { Component, OnInit } from '@angular/core';
import {SpotifyService} from '../../services/spotify.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styles: []
})
export class HomeComponent implements OnInit {
  newSongs: any[] = [];
  loading: boolean;
  error = false;
  msgError: string;

  constructor(private _spotifyService: SpotifyService) {
    this.loading = true;
    this._spotifyService.getNewReleases().subscribe(
      (response: any) => {
        this.newSongs = response;
        this.loading = false;
      },
      err => {
        this.loading = false;
        this.error = true;
        this.msgError = err.error.error.message;
      }
    );
  }

  ngOnInit() {
  }
}
