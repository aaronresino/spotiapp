import { Component } from '@angular/core';
import {ActivatedRoute, Params} from '@angular/router';
import {SpotifyService} from '../../services/spotify.service';

@Component({
  selector: 'app-artist',
  templateUrl: './artist.component.html',
  styles: []
})
export class ArtistComponent {
  artist: any = {};
  tracks: any[] = [];
  loading: boolean;
  error = false;
  msgError: string;

  constructor(private _route: ActivatedRoute, private _spotifyService: SpotifyService) {
    this.loading = true;
    this._route.params.subscribe((params: Params) => {
      this.getArtist(params['id']);
      this.getTopTracks(params['id']);
    });
  }

  getArtist(id: string) {
    this._spotifyService.getArtist(id).subscribe(
      data => {
        this.artist = data;
        this.loading = false;
      },
      err => {
        this.error = true;
        this.msgError = err.error.error.message;
        this.loading = false;
      }
    );
  }

  getTopTracks(id: string) {
    this._spotifyService.getTopTracks(id).subscribe(
      data => {
        this.tracks = data;
        this.loading = false;
      },
      err => {
        this.error = true;
        this.msgError = err.error.error.message;
        this.loading = false;
      }
    );
  }
}
